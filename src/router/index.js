import { createRouter, createWebHashHistory } from 'vue-router'

import HomeView from '../views/HomeView.vue';
import IssView from '../views/IssView.vue';
import GpsView from '../views/GpsView.vue';
import FilteredView from '../views/FilteredView.vue';

const routes = [
    {
        path: "/",
        component: HomeView,
    },
    {
        path: "/iss",
        component: IssView,
    },
    {
        path: "/gps",
        component: GpsView,
    },
    {
        path: "/filtered",
        component: FilteredView,
    }
]

const router = createRouter({
    history: createWebHashHistory(), // process.env.BASE_URL
    routes
})

export default router