export function getDateTimeString(localDatetime) {
    let date = new Date(localDatetime);
    let nonTimezoneDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000);
    return nonTimezoneDate.toISOString().split('.')[0];
}