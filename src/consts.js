export const API_URL = "http://localhost:8080";

export const API_ROUTES = {
    filter: {
        iss: `${API_URL}/filter/iss`,
        gps: `${API_URL}/filter/gps`,
        list: `${API_URL}/filter/list`,
    }
}